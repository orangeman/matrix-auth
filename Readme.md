
# matrix-auth

authenticate with [passport](https://github.com/jaredhanson/passport/wiki/Strategies) oAuth
to use a [matrix](https://matrix.org) [synapse](https://github.com/matrix-org/synapse) server

* [shared secret registration](https://github.com/matrix-org/synapse/blob/master/docs/admin_api/register_api.rst)
to register new accounts

* [shared secret authentication](https://github.com/devture/matrix-synapse-shared-secret-auth)
for existing accounts

* [basic-vue-chat](https://github.com/jmaczan/basic-vue-chat) vue chat ui component

----------------

### setup

    cp config.yaml.example config.yaml
    vue build Chat.vue
    node index.js

Minimal frontend (static single page web app) gets compiled into `dist/`.
Login button [opens](https://gitlab.com/orangeman/matrix-auth/-/blob/73d568718bc4c0cb18120bc0ab1a3212b3a10785/Chat.vue#L21) a browser tab to do the oAuth [dance](https://gitlab.com/orangeman/matrix-auth/-/blob/73d568718bc4c0cb18120bc0ab1a3212b3a10785/index.js#L24)
and then [register/authenticate](https://gitlab.com/orangeman/matrix-auth/-/blob/73d568718bc4c0cb18120bc0ab1a3212b3a10785/matrix.js#L7-13) with synapse matrix server.
The matrix device auth token gets [written](https://gitlab.com/orangeman/matrix-auth/-/blob/73d568718bc4c0cb18120bc0ab1a3212b3a10785/index.js#L29-33) to localstorage and the browser tab closes itself.
The (original) chat app (browser tab) gets [notified](https://gitlab.com/orangeman/matrix-auth/-/blob/73d568718bc4c0cb18120bc0ab1a3212b3a10785/Chat.vue#L48) about the localstorage (auth token) and connects a matrix client to start chatting...
