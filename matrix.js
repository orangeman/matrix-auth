config = require('js-yaml').safeLoad(require('fs').readFileSync('config.yaml'))
const crypto = require('crypto')
const axios = require('axios')

module.exports = { auth }

async function auth(name) {
  try {
    return await login(name)
  } catch(e) {
    return await register(name)
  }
}

axios.defaults.baseURL = config.homeserver

async function login(name) {
  mac = crypto.createHmac('sha512', config.authentication_shared_secret)
  mac.update('@' + name + ':' + config.homeserver.split('matrix.')[1])
  res = await axios.post('/_matrix/client/r0/login', {
    password: mac.digest('hex'),
    type: 'm.login.password',
    user: name
  })
  return res.data
}

async function register(name, pass) {
  r = await axios.get('/_synapse/admin/v1/register')
  if (!pass) pass = crypto.randomBytes(42).toString('hex')
  mac = crypto.createHmac('sha1', config.registration_shared_secret);
  mac.update(r.data.nonce + '\x00' + name + '\x00' + pass + '\x00' + 'notadmin')
  user = await axios.post('/_synapse/admin/v1/register', {
    nonce: r.data.nonce,
    username: name,
    password: pass,
    admin: false,
    mac: mac.digest('hex')
  })
  return user.data
}
