config = require('js-yaml').safeLoad(require('fs').readFileSync('config.yaml'))

const passport = require('passport')
const express = require('express')
const matrix = require('./matrix')

app = express()
app.use(express.static('dist'))

app.use(passport.initialize())
passport.serializeUser((user, done) => done(null, user.username))
passport.deserializeUser((id, done) => done(err, { username: id }))

passport.use(new (require('passport-github').Strategy)({
    clientID: config.github_client_id,
    clientSecret: config.github_client_secret,
    callbackURL: config.home + '/github/callback'
  },
  (accessToken, refreshToken, profile, cb) => {
    return cb(null, profile)
  }
))

app.get('/github', passport.authenticate('github'))

app.get('/github/callback', passport.authenticate('github'), (req, res) => {
  matrix.auth(req.user.username).then(auth => {
    console.log('callback', auth)
    res.send(`<script>localStorage.setItem("matrix", JSON.stringify({
      baseUrl: "${config.homeserver}",
      accessToken: "${auth.access_token}",
      userId: "${auth.user_id}" }));
      window.close(); </script>`)
  })
})

app.listen(3000, () => console.log('server started'))
